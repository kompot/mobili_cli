from setuptools import setup
import os

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name="mobili_cli",
    version="0.1",
    packages=["mobili_cli", "mobili_cli.commands", "mobili_cli.utils"],
    include_package_data=True,
    install_requires=required,
    entry_points="""
        [console_scripts]
        mobili_cli=mobili_cli.cli:cli
    """,
)
