#!/usr/bin/env python3

import sys
import feedparser
from pprint import pp

from mobili_cli.event import Event, parseRSS

try:
    url = sys.argv[1]

except ValueError:
    raise SystemExit('Usage: load-rss-feed.py <url of rss feed>')

f = feedparser.parse(url)
for e in f['entries']:
    pp(e)
    event = parseRSS(e)
    pp(str(event))
