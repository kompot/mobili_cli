#!/usr/bin/env python3

import sys
import json
from sgqlc.endpoint.http import HTTPEndpoint


mutation = """
mutation Login($email: String, $password: String!) {
  login(email: $email, password: $password) {
    accessToken
    refreshToken
    user {
      id
      email
      role
    }
  }
}
"""

try:
    username, password = sys.argv[1:]
except ValueError:
    raise SystemExit('Usage: <username|email> <password>')

