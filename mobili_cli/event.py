#!/usr/bin/env python3

from pprint import pp
from datetime import datetime
from metadata_parser import MetadataParser

class Event:
    status = None

    def __init__(self, title, time, link):
        self.title = title
        self.time = time
        self.link = link

    def setStatus(self, status):
        self.status = status

    def setLocation(self, location):
        self.location = location

    def setImageFromLink(self, link):
        page = MetadataParser(self.link)
        self.image = page.get_metadata_link('image')

    def __str__(self):
        return str({
            'title': self.title,
            'time': self.time,
            'link': self.link,
            'status': self.status,
            'image': self.image
        })

def parseRSS(entry):
    for k in ['title', 'ical_dtstart', 'link']:
        if k not in entry:
            raise ValueError(f"{k} is a required event parameter")

    title, time, link = (entry.title, datetime.fromisoformat(entry['ical_dtstart']), entry.link)
    ev = Event(title, time, link)

    if 'ical_status' in entry:
        ev.setStatus(entry['ical_status'])

    if 'ical_location' in entry:
        ev.setLocation(entry['ical_location'])

    ev.setImageFromLink(ev.link)

    return ev
