from mobili_cli.cli import pass_environment

import click


@click.command("test", short_help="Testing mobili cli.")
@click.argument("name", required=False, type=click.STRING)
@pass_environment
def cli(ctx, name):
    """Greeting."""
    if name is None:
        name = "Mobilizon"
    # ctx.log(f"Greeting printed.")
    click.echo("Hello " + name + "!")
